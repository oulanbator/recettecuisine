FROM openjdk:11
ADD /target/recettes-0.0.1-SNAPSHOT.jar app.jar
RUN sh -c 'touch /app.jar'
CMD ["java", "-jar", "/app.jar"]
EXPOSE 8080
