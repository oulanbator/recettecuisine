package epsi.devops.recettes;

import epsi.devops.recettes.entity.Recette;
import epsi.devops.recettes.service.RecetteService;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertTrue;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = RecettesApplication.class)
@TestPropertySource(locations = "classpath:integration.properties")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RecettesApplicationTests {

    @Autowired
    private RecetteService recetteService;

    @Test
    public void a_creerUneRecette() throws Exception {
        System.out.println("\n\nTEST 1 : Création d'une recette :");
        Recette recette = new Recette();
        recette.setTitre("Salade");
        recette.setTempsPreparation("5min");
        recette.setIngredients("Salade, Tomates, Oignons");
        recette.setEtapesPreparation("Tout mélanger, c'est prêt");
        recetteService.saveRecette(recette);

        List<Recette> recettes = recetteService.getAllRecettes();
        Recette derniereRecette = recettes.get(recettes.size() - 1);
        System.out.println(derniereRecette.toString());

        assertTrue(Objects.equals(derniereRecette.getTitre(), recette.getTitre()));
    }

    @Test
    public void b_modifierRecette() {
        System.out.println("\n\nTEST 2 : Modification de la recette avec un nouveau titre :");
        String newTitre = "Un titre de recette différent";
        List<Recette> recettes = recetteService.getAllRecettes();
        Recette derniereRecette = recettes.get(recettes.size() - 1);
        String titreInitial = derniereRecette.getTitre();
        Long id = derniereRecette.getId();

        derniereRecette.setTitre(newTitre);
        recetteService.saveRecette(derniereRecette);

        Recette reloaded = recetteService.findRecetteById(id);
        System.out.println(reloaded.toString());
        assertTrue(Objects.equals(reloaded.getTitre(), newTitre) && !newTitre.equals(titreInitial));
    }

    @Test
    public void c_listerRecettes() {
        System.out.println("\n\nTEST 3 : Liste des recettes présentes dans la base de test : ");
        List<Recette> recettes = recetteService.getAllRecettes();

        for (Recette r : recettes) {
            System.out.println(" - " + r.getTitre());
        }

        assertTrue(recettes.size() > 0);
    }

    @Test
    public void d_deleteRecette() {
        System.out.println("\n\nTEST 4 : Supprimer une recette :");

        List<Recette> recettesDebut = recetteService.getAllRecettes();
        System.out.println("\nRecettes initiales :");
        for (Recette r : recettesDebut) {
            System.out.println(" - " + r.getTitre());
        }

        Recette derniereRecette = recettesDebut.get(recettesDebut.size() - 1);
        recetteService.deleteRecette(derniereRecette);

        List<Recette> recettesFin = recetteService.getAllRecettes();
        System.out.println("\nRecettes finales :");
        for (Recette r : recettesFin) {
            System.out.println(" - " + r.getTitre());
        }
        assertTrue(recettesFin.size() < recettesDebut.size());
    }

}
