package epsi.devops.recettes.entity;

import javax.persistence.*;

@Entity
public class Recette {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String titre;
    private String tempsPreparation;

    @Column(length = 1000)
    private String ingredients;

    @Column(length = 1000)
    private String etapesPreparation;

    public Recette() {
    }

    public Recette(String titre, String tempsPreparation, String ingredients, String etapesPreparation) {
        this.titre = titre;
        this.tempsPreparation = tempsPreparation;
        this.ingredients = ingredients;
        this.etapesPreparation = etapesPreparation;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getTempsPreparation() {
        return tempsPreparation;
    }

    public void setTempsPreparation(String tempsPreparation) {
        this.tempsPreparation = tempsPreparation;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getEtapesPreparation() {
        return etapesPreparation;
    }

    public void setEtapesPreparation(String etapesPreparation) {
        this.etapesPreparation = etapesPreparation;
    }

    @Override
    public String toString() {
        return "Recette{" +
                "id=" + id +
                ", titre='" + titre + '\'' +
                ", tempsPreparation='" + tempsPreparation + '\'' +
                ", ingredients='" + ingredients + '\'' +
                ", etapesPreparation='" + etapesPreparation + '\'' +
                '}';
    }
}
