package epsi.devops.recettes.controller;

import epsi.devops.recettes.entity.Recette;
import epsi.devops.recettes.service.RecetteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class RecetteController {

    private final RecetteService recetteService;

    @Autowired
    public  RecetteController(RecetteService recetteService) {
        this.recetteService = recetteService;
    }

    @GetMapping("/")
    public String home() {
        return "redirect:/recettes";
    }

    @GetMapping("/recettes")
    public String getRecettes(Model model) {
        List<Recette> recettes = recetteService.getAllRecettes();
        model.addAttribute("recettes", recettes);
        return "recettes";
    }

    @GetMapping("/recette/{id}")
    public String getRecettes(@PathVariable String id, Model model) {
        Recette recette = recetteService.findRecetteById(Long.valueOf(id));
        if (recette != null) {
            model.addAttribute("recette", recette);
            return "recette";
        } else {
            return "404";
        }
    }

    @GetMapping("/ajouter")
    public String ajouterRecette(Model model) {
        Recette recette = new Recette();
        model.addAttribute("recette", recette);
        return "recetteForm";
    }

    @PostMapping(value = "/createRecette", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String createRecette(Recette recette) {
        if (recette.getId() == null) {
            recetteService.saveRecette(recette);
            return "redirect:/recettes";
        } else {
            Recette dbRecette = recetteService.findRecetteById(recette.getId());
            if (dbRecette != null) {
                recetteService.updateRecette(dbRecette, recette);
                return "redirect:/recette/" + dbRecette.getId();
            } else {
                return "404";
            }
        }
    }

    @GetMapping("/edit/{id}")
    public String editerRecette(@PathVariable String id, Model model) {
        Recette recette = recetteService.findRecetteById(Long.valueOf(id));
        if (recette != null) {
            model.addAttribute("recette", recette);
            return "recetteForm";
        } else {
            return "404";
        }
    }

    @GetMapping("/delete/{id}")
    public String supprimerRecette(@PathVariable String id) {
        Recette recette = recetteService.findRecetteById(Long.valueOf(id));
        if (recette != null) {
            recetteService.deleteRecette(recette);
            return "redirect:/recettes";
        } else {
            return "404";
        }
    }

}
