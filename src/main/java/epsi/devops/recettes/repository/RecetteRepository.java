package epsi.devops.recettes.repository;

import epsi.devops.recettes.entity.Recette;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecetteRepository extends PagingAndSortingRepository<Recette, Long> {
    List<Recette> findAll();
}
