package epsi.devops.recettes.service;

import epsi.devops.recettes.entity.Recette;
import epsi.devops.recettes.repository.RecetteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RecetteService {
    private final RecetteRepository repository;

    @Autowired
    public RecetteService(RecetteRepository repository) {
        this.repository = repository;
    }


    public List<Recette> getAllRecettes() {
        return repository.findAll();
    }


    public Recette findRecetteById(Long id) {
        Optional<Recette> recette = repository.findById(id);
        return recette.orElse(null);
    }

    public void saveRecette(Recette recette) {
        this.repository.save(recette);
    }

    public void updateRecette(Recette dbRecette, Recette updatedRecette) {
        dbRecette.setTitre(updatedRecette.getTitre());
        dbRecette.setTempsPreparation(updatedRecette.getTempsPreparation());
        dbRecette.setIngredients(updatedRecette.getIngredients());
        dbRecette.setEtapesPreparation(updatedRecette.getEtapesPreparation());
        this.saveRecette(dbRecette);
    }

    public void deleteRecette(Recette recette) {
        repository.delete(recette);
    }
}
