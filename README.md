**Application de recettes de cuisine.**

**Technologies :**

- Java (Springboot)
- Thymeleaf (Springboot MVC)
- Base de données MySQL
- ORM : Hibernate
- Gitlab CI/CD
- Docker

**Classe : EPSI B3 C3**

**Etudiants :**
- Mathis ALLEN
- Thomas ANDRES
- Sarah MACHADO
- Victor MATHERON

**DEPLOYMENT : Run Conteneurs depuis Registre distant :**

docker network create devops

docker run -it -p 3306:3306 --rm --name mysql-devops --network devops -v store:/var/lib/mysql registry.gitlab.com/oulanbator/recettecuisine/mysql-devops:main

docker container run --network devops -p 8080:8080 --name recetteapp --rm registry.gitlab.com/oulanbator/recettecuisine:main


**Lancer l'appli :**

http://localhost:8080/recettes


**Connexion JDBC :**

jdbc:mysql://mysql-devops:3306/recettes?allowPublicKeyRetrieval=true&createDatabaseIfNotExist=true&useSSL=false

username=root

password=devops123456


**Clean docker :**

Stopper les conteneurs les supprimera automatiquement si l'on se sert des commandes ci-dessus.
Pour nettoyer complètement docker, il faudra en plus..

- supprimer le network et le volume de persistence créés :

docker volume rm store

docker network rm devops

- identifier et supprimer les images téléchargées :

docker images

docker image rm [id image springboot]

docker image rm [id image mysql]


**DEVELOPMENT : Run Conteneur Springboot en local pour tests (se placer dans le repo) :**

docker build -t recettecuisine .

docker run -it -p 8080:8080 --rm --name RecetteApp recettecuisine
